from tomcat
maintainer Kat McIvor

copy target/hello-scalatra.war /usr/local/tomcat/webapps/hello-scalatra.war
copy tomcat-users.xml /usr/local/tomcat/conf/

expose 8080

CMD ["catalina.sh", "run"]
